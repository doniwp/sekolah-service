<?php

/**
 * Created by PhpStorm.
 * User: doniw
 * Date: 11/09/2017
 * Time: 20.21
 */
namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\Http\Controllers\PhpSerial;
use App\Models\App\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Mockery\CountValidator\Exception;
use Tymon\JWTAuth\Facades\JWTAuth;

class MainCtrl extends Controller {
    public function refreshToken() {
    }

    public function login(Request $request) {
        if (!Auth::attempt(['siswa_username' => $request->input('username'), 'password' => $request->input('password')])) {

            $message = [
                'timestamp' => time(),
                'status' => 401,
                'error' => 'Authentication Failed',
                'message' => 'Username doesn\'t exist or Password doesn\'t match.',
                'path' => '/api/site/login'
            ];

            return response()->json($message, 401);
        }

        $siswa_id = Auth::user()->siswa_id;

        $extData = [
            'data' => [
                'id' => $siswa_id,
            ],
        ];

        try {
            $token = JWTAuth::fromUser(Auth::user(), $extData);
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        return response()->json([
            'token' => $token,
            'id' => $siswa_id
        ]);
    }

    public function change(Request $request) {
        $payload = JWTAuth::parseToken()->getPayload();
        $id = $payload->get('data')['id'];
        $password = $request->input('password');

        $hash = bcrypt($password);

        DB::statement("UPDATE dst_siswa SET siswa_password = '" . $hash . "' WHERE siswa_id = " . $id . " ");

        return response()->json('Success changed password.');
    }

    public function logout() {
        try {
            JWTAuth::invalidate(JWTAuth::getToken());
        } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

        }
    }

    public function getAbsensi(Request $request) {
        $payload = JWTAuth::parseToken()->getPayload();
        $id = $payload->get('data')['id'];
        $date = $request->input('date');

        $absensi = DB::select("SELECT c.mapel_nama, a.absen_keterangan
                            FROM dst_absen a
                            JOIN dst_siswa b ON b.siswa_id=a.fk_siswa_id
                            JOIN dst_mapel c ON c.mapel_id=a.fk_mapel_id
                            WHERE a.fk_siswa_id = ? AND a.absen_tanggal= ?", [$id, $date]);

        if (empty($absensi)) {
            return response()->json(["message" => "Not Found."], 404);
        }

        return $absensi;
    }

    public function getPelanggaran(Request $request) {
        $payload = JWTAuth::parseToken()->getPayload();
        $id = $payload->get('data')['id'];
        $date = $request->input('date');

        $pelanggaran = DB::select("SELECT b.pelanggaran_kode, b.pelanggaran_nama, b.pelanggaran_point
                            FROM dst_point a
                            JOIN dst_pelanggaran b ON b.pelanggaran_id=a.fk_pelanggaran_id
                            WHERE a.fk_siswa_id = ? AND a.point_tanggal = ?", [$id, $date]);

        if (empty($pelanggaran)) {
            return response()->json(["message" => "Not Found."], 404);
        }

        return $pelanggaran;
    }

    public function getCatatan() {
        $payload = JWTAuth::parseToken()->getPayload();
        $id = $payload->get('data')['id'];

        $catatan = DB::select("SELECT *
                            FROM dst_siswa
                            WHERE siswa_id = ?", [$id]);

        if (empty($catatan)) {
            return response()->json(["message" => "Not Found."], 404);
        }

        return $catatan;
    }

    public function getKeuangan(Request $request) {
        $payload = JWTAuth::parseToken()->getPayload();
        $id = $payload->get('data')['id'];
        $month = $request->input('month');
        $year = $request->input('year');

        $keuangan = DB::select("SELECT *
                                FROM dst_keuangan a
                                WHERE a.fk_siswa_id = ? AND MONTH(a.keuangan_deadline) = ? AND YEAR(a.keuangan_deadline) = ?", [$id, $month, $year]);

        if (empty($keuangan)) {
            return response()->json(["message" => "Not Found."], 404);
        }

        return $keuangan;
    }

    public function getKelas() {
        $kelas = DB::select("SELECT * FROM dst_kelas");

        if (empty($kelas)) {
            return response()->json(["message" => "Not Found."], 404);
        }

        return $kelas;
    }

    public function getNilai(Request $request) {
        $payload = JWTAuth::parseToken()->getPayload();
        $id = $payload->get('data')['id'];
        $kelas = $request->input('kelas');

        $nilai = DB::select("SELECT b.mapel_nama, a.nilai_uts,a.nilai_uas,a.nilai_ta
                                FROM dst_nilai a
                                JOIN dst_mapel b ON b.mapel_id=a.fk_mapel_id
                                WHERE a.fk_siswa_id = ? AND a.fk_kelas_id = ?", [$id, $kelas]);

        if (empty($nilai)) {
            return response()->json(["message" => "Not Found."], 404);
        }

        return $nilai;
    }
}