<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return "Your IP has been recorded. Authorities has been notified.";
});

/*//////////////
ALL
//////////////*/
Route::post('/login', 'App\MainCtrl@login');
Route::get('/kelas', 'App\MainCtrl@getKelas');

/*//////////////
LOGGED IN
//////////////*/
Route::group([
    'middleware' => 'jwt.auth'
], function () {
    Route::get('/refresh', 'App\MainCtrl@refreshToken');
    Route::post('/change', 'App\MainCtrl@change');
    Route::get('/logout', 'App\MainCtrl@logout');
    Route::post('/absensi', 'App\MainCtrl@getAbsensi');
    Route::post('/pelanggaran', 'App\MainCtrl@getPelanggaran');
    Route::get('/catatan', 'App\MainCtrl@getCatatan');
    Route::post('/keuangan', 'App\MainCtrl@getKeuangan');
    Route::post('/nilai', 'App\MainCtrl@getNilai');
});
