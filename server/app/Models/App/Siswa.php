<?php

/**
 * Created by PhpStorm.
 * User: doniw
 * Date: 11/09/2017
 * Time: 20.08
 */
namespace App\Models\App;

use App\Models\ExtendModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;

class Siswa extends ExtendModel implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract {
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'dst_siswa';
    public $timestamps = false;
    protected $primaryKey = 'siswa_id';
    protected $guarded = [];
    protected $hidden = [
        'siswa_password',
    ];
    protected $rules = [
        'siswa_nis' => 'required',
        'siswa_nama' => 'required',
        'fk_jurusan_id' => 'required',
        'fk_kelas_id' => 'required',
        'fk_agama_id' => 'required',
        'siswa_ortu' => 'required',
        'siswa_alamat' => 'required',
        'siswa_telp' => 'required',
        'siswa_username' => 'required',
        'siswa_password' => 'required',
        'siswa_point' => '',
        'siswa_keterangan' => '',
    ];

    public function getAuthPassword() {
        return $this->siswa_password;
    }

    public function getRememberToken() {
        return null;
    }

    public function setRememberToken($value) {
        // not supported
    }

    public function getRememberTokenName() {
        return null; // not supported
    }
}