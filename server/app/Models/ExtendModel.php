<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class ExtendModel extends Model {
    protected $rules = array();
    protected $errors;

    public function validate($data) {
        $v = Validator::make($data, $this->rules);
        if ($v->fails()) {
            $this->errors = $v->errors();
            return false;
        }
        return true;
    }

    public function errors() {
        return $this->errors;
    }
}